## Linux Streaming Scripts

This is just a collection of scripts that I use when streaming on Linux.

The `stream-setup.sh` script is an all-in-one script that sets up the following:
- Two audio sinks complete with loopback devices that connect them.
    - One sink for stream output only
    - One sink for audio that gets shared
    - Output to only you is kept as the default
    - App audio can then easily be adjusted using the Playback tab in PulseAudio's Volume Control GUI
- Noise reduction for the microphone via WebRTC using PulseAudio's built-in `module-echo-cancel`.
- Video loopback device using the `v4l2loopback` kernel module and `ffmpeg` to allow multiple apps to access the camera.
    - Includes a second loopback device for OBS Studio's virtual output.

The `mic-noise-redux.sh` script is just a stripped-down version of the `stream-setup.sh` script that only contains the echo cancel module part. Also lacks the commentary of the original.

The `offload-to-nvidia.sh` and `offload-to-nvidia-nogl.sh` scripts are to automate loading apps using nVidia's experimental offloading support introduced in 435.xx proprietary drivers. More info on it can be found in the readme for nVidia's own drivers. The latest online version can be found here:
https://download.nvidia.com/XFree86/Linux-x86_64/470.57.02/README/primerenderoffload.html

The `nogl` variant is generally the best route unless you're running an app that uses OpenGL. Some apps might also run into problems with forcing OpenGL offloading, in which case, using the `nogl` variant might still let them use the nVidia GPU anyway.

As of this writing, I can confirm that Kodi is one such app. It will refuse to render if OpenGL offloading is forced, but will still offload rendering if the `nogl` version is used instead.
