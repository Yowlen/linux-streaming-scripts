#!/bin/bash

echocancel="EchoCancelMic"
#micsource="PulseEffects_mic.monitor"
#micsource="bluez_source.FC_58_FA_F3_B4_15.headset_head_unit"
#micsource="alsa_input.pci-0000_28_00.3.analog-stereo"
micsource="alsa_input.usb-Samson_Technologies_Samson_Go_Mic-00.analog-stereo"
pactl unload-module module-echo-cancel 2>/dev/null
pactl load-module module-echo-cancel aec_method=webrtc source_name="$echocancel" source_master="$micsource"
pacmd update-source-proplist "$echocancel" device.description="$echocancel"
pacmd set-default-source "$echocancel"

