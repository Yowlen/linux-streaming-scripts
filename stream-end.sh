#!/bin/bash

echo "This will unload specific module numbers."
echo "It DOES NOT search for the modules."
echo "If your configuration differs from the following,"
echo "press CTRL-C now and manually unload them or reboot."
echo ""
echo "The specific module IDs this unloads are:"
echo "30, 32 through 36"
echo ""
read -p "If this is correct, press any key to unload them now. " -n1 -s
echo ""

pactl unload-module 37
pactl unload-module 36
pactl unload-module 35
pactl unload-module 34
pactl unload-module 33
pactl unload-module 31

