#!/bin/bash

nullsink="RecordingSink"
discord=$(pacmd list-source-outputs | grep -B 15 'WEBRTC' | grep '  index' | cut -f6 -d' ')
nullsinkmon=$(pacmd list-sources | grep -B 15 "$nullsink" | grep '  index' | cut -f6 -d' ')
pactl move-source-output $discord $nullsinkmon

