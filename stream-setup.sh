#!/bin/bash

echo "Preparing to set up audio."
echo "If you're using PulseEffects, make sure it's set to NOT process all inputs."
echo ""
echo "Also, if this is your first time using the script or you've made a recent"
echo "change related to your audio hardware, press CTRL-C now, then open the script"
echo "in a text editor and set the variables according to the instructions inside."
echo ""
read -p "When you're ready, press any key to continue. " -n1 -s
echo ""

# Audio device variables
#
# The null sink and echo cancel names can be changed based on personal preference.
nullsink="RecordingSink"
nullsinksplitter="SharedAudio"
echocancel="EchoCancelMic"

# The mic source and audio output variables must match an exact string based on your system's respective inputs/outputs.
# You can use "pactl list short sources" and "pactl list short sinks" to find the respective names.
# It's usually one of the ones near the top that doesn't have ".monitor" at the end of the name.

#micsource="PulseEffects_mic.monitor"
#micsource="bluez_source.FC_58_FA_F3_B4_15.headset_head_unit"
#micsource="alsa_input.pci-0000_28_00.3.analog-stereo"
micsource="alsa_input.usb-Samson_Technologies_Samson_Go_Mic-00.analog-stereo"
audiooutput="PulseEffects_apps"
speakers="alsa_output.pci-0000_28_00.3.analog-stereo"

# Our microphone overrides things when it's plugged in and it gets set to the default speakers, so we need to fix that.
pactl set-default-sink "$speakers"

# Create our special output sink
pactl load-module module-null-sink sink_name="$nullsink"
pacmd update-sink-proplist "$nullsink" device.description="$nullsink"

# Create our special splitter sink that we can route all the app audio to that we want to share
pactl load-module module-null-sink sink_name="$nullsinksplitter"
pacmd update-sink-proplist "$nullsinksplitter" device.description="$nullsinksplitter"

# Grab the input monitor for the splitter to properly assign it as the source
nullsinksplitmon=$(pactl list sources | grep -B 15 "$nullsinksplitter" | grep "Source #" | cut -f2 -d '#')

# Send a copy of the splitter sink to the special output sink
pactl load-module module-loopback latency_msec=1 source="$nullsinksplitmon" sink="$nullsink"
# And send a copy of the splitter sink to the desktop speakers/headphones
pactl load-module module-loopback latency_msec=1 source="$nullsinksplitmon" sink="$audiooutput"

# Now to set up the microphone.
#
# Add background noise reduction to the mic and set it as the new default mic
pactl unload-module module-echo-cancel 2>/dev/null # Just in case an existing echo-cancel module is already loaded
pactl load-module module-echo-cancel aec_method=webrtc source_name="$echocancel" source_master="$micsource"
pacmd update-source-proplist "$echocancel" device.description="$echocancel"
pacmd set-default-source "$echocancel"

# Send a copy of the new mic source to the special output sink
pactl load-module module-loopback latency_msec=1 source="$echocancel" sink="$nullsink"

# All that needs to be done now is to set the individual apps to use the appropriate input/output sources
# from within PulseAudio's Volume Control app.
# Set Discord to use the special output sink
#discord=$(pacmd list-source-outputs | grep -B 15 'WEBRTC' | grep '  index' | cut -f6 -d' ')
#nullsinkmon=$(pacmd list-sources | grep -B 15 "$nullsink" | grep '  index' | cut -f6 -d' ')
#pactl move-source-output $discord $nullsinkmon

echo "Preparing video setup."
echo "Make sure the webcam is plugged in and that OBS Studio is closed."
echo "Note that if your physical webcam isn't located at /dev/video0,"
echo "you'll need to adjust the script to get it to work."
echo ""
echo "You will need to enter your root password to activate the video loopback driver."
echo "It will create two new video devices located at /dev/video4 and /dev/video5"
echo "video4 is for OBS' virtual output and video5 is for the virtual webcam."
echo ""
read -p "Press any key to continue, or CTRL-C to skip webcam setup. " -n1 -s
echo ""
sudo modprobe -r v4l2loopback
sudo modprobe v4l2loopback video_nr=4,5 card_label="OBS Virtual Output","Virtual Webcam" exclusive_caps=1
nohup "ffmpeg -input_format mjpeg -framerate 30 -video_size 1280x720 -i /dev/video0 -f v4l2 -pix_fmt yuv420p /dev/video5" > /dev/null &

